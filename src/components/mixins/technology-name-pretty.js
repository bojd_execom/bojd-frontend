export default {
    computed: {
        technologyPretty() {
            if (this.lambda.technology == 'csharp') {
                return 'C#';
            } else if (this.lambda.technology == 'python') {
                return 'Python';
            }
        }
    }
}