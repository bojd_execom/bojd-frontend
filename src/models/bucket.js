import assign from 'lodash/assign';
import cloneDeep from 'lodash/cloneDeep';

const defaultAttributes = {
    bucketId: -1,
    name: '',
    size: '1',
    spaceLeft: 0,
    publicFlag: "true"
}

export class Bucket {

    constructor(json) {
        assign(this, cloneDeep(defaultAttributes), json);
    }

}