import assign from 'lodash/assign';
import cloneDeep from 'lodash/cloneDeep';

const defaultAttributes = {
    id: -1,
    lambdaName: '',
    technology: 'csharp'
}

export class Lambda {

    constructor(json) {
        assign(this, cloneDeep(defaultAttributes), json);
    }

}