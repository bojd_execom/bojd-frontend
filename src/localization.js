// If using a module system (e.g. via vue-cli), import Vue and VueI18n and then call Vue.use(VueI18n).
import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n)

// Ready translated locale messages
const messages = {
    en: {
        message: {
            hello: 'Welcome to BOJD cloud services!'
        },
        info: {
            login: {
                success: 'You are successfuly logged in!',
            },
            register: {
                success: 'Successfuly registered!'
            }
        },
        error: {
            login: {

            }
        }
    },
    sr: {
        message: {
            hello: 'Добродошли на сајт БОЈД cloud сервиса!'
        },
        info: {
            login: {
                success: 'Успешно сте се улоговали!',
            },
            register: {
                success: 'Успешно сте се регистовали!'
            }
        },
        error: {
            login: {
                
            }
        }
    }
}
  
  // Create VueI18n instance with options
  const i18n = new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
  })

  export default i18n;