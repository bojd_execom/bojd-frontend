const API_ENDPOINT = {
    dev: 'http://localhost:55001/api/',
    production: 'http://localhost:55001/api/'
}

const lockedPagesForGuest = [
    '/bucket',
    '/lambda_homepage',
    '/create_lambda',
    '/create_bucket',
    '/lambda'
]
const lockedPagesForUser = [
    '/login',
    '/register'
]

function getApiEndpoint() {
    return API_ENDPOINT[ENVIRONMENT];
}

export { getApiEndpoint, lockedPagesForGuest, lockedPagesForUser };