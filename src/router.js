
import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import LoginForm from './components/auth/LoginForm.vue';
import RegisterForm from './components/auth/RegisterForm.vue';
import authService from './services/auth-service.js';
import BucketPage from './components/bucket/BucketPage.vue';
import BucketCreateForm from './components/bucket/BucketCreateForm.vue';
import LambdaHomepage from './components/lambda/LambdaHomepage.vue';
import LambdaCreateWizard from './components/lambda/LambdaCreateWizard.vue';
import LambdaPage from './components/lambda/LambdaPage.vue';
import { lockedPagesForGuest, lockedPagesForUser } from './constants/index.js';

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/login',
      component: LoginForm
    },
    {
      path: '/register',
      component: RegisterForm
    },
    {
      path: '/bucket/:id',
      component: BucketPage
    },
    {
      path: '/create_bucket',
      component: BucketCreateForm
    },
    {
      path: '/edit_bucket/:id',
      component: BucketCreateForm
    },
    {
      path: '/lambda_homepage',
      component: LambdaHomepage
    },
    {
      path: '/create_lambda',
      component: LambdaCreateWizard
    },
    {
      path: '/lambda/:id',
      component: LambdaPage
    },
    {
      path: '/edit_lambda/:id',
      component: LambdaCreateWizard
    },
  ]
})

router.beforeEach((to, from, next)=>
{
  if (authService.isAuthenticated())
  {
    if (lockedPagesForUser.filter(lockedPage => lockedPage == to.path).length < 1)
    {
      next();
    }
    else next('/');
  }
  else
  {
    if (lockedPagesForGuest.filter(lockedPage => to.path.startsWith(lockedPage)).length < 1)
    {
      next();
    }
    else next('/');
  }
});  

export default router;