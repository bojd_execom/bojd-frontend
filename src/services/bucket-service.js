import { getApiEndpoint } from '../constants/index.js';
import { Bucket } from '../models/bucket.js';
import axios from 'axios';

export default {
    getAll() {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.get(getApiEndpoint() + 'bucket', { headers: headers })
                .then(({ data }) => {
                    let buckets = []
                    for(let bucketJson of data) {
                        buckets.push(new Bucket(bucketJson));
                    }
                    resolve(buckets);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    get(id) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.get(getApiEndpoint() + 'bucket/' + id, { headers: headers })
                .then(({ data }) => {
                    resolve(new Bucket(data));
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        })
    },

    create(bucket) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.post(getApiEndpoint() + 'bucket', bucket, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        })
    },

    edit(bucket) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.put(getApiEndpoint() + 'bucket/', bucket, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        })
    },

    uploadFile(bucketInfo, file) {
        console.log(bucketInfo)
        let headers = { 
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value 
        };
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append('file', file);
            formData.append('bucketId', bucketInfo.bucketId);
            axios.post(getApiEndpoint() + 'bucket/files', formData, { headers: headers })
                .then(response => {
                    console.log('SUCCESS!!');
                    resolve(response);
                })
                .catch(({ response }) => {
                    console.log('FAILURE!!');
                    reject(response.data);
                });
        });    
    },

    getFiles(bucketId) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.get(getApiEndpoint() + 'bucket/' + bucketId + '/files', { headers: headers })
                .then(({ data }) => {
                    resolve(data);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    delete(bucketId) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.delete(getApiEndpoint() + 'bucket/' + bucketId, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    downloadFile(bucketId, fileName) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.get(getApiEndpoint() + 'bucket/' + bucketId + '/file/' + fileName, { headers: headers })
                .then(({ data }) => {
                    var mime = require('mime-types');
                    var blob = this.b64toBlob(data.content, mime.lookup(data.name), 512);
                    const url = window.URL.createObjectURL(blob);
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', data.name);
                    document.body.appendChild(link);
                    link.click();
                    resolve();
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
      
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
      
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);
      
          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }
      
          var byteArray = new Uint8Array(byteNumbers);
      
          byteArrays.push(byteArray);
        }
      
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }

}