import axios from 'axios';
import { getApiEndpoint } from '../constants/index.js';
import router from '../router.js';

export default {
    login(user) {
        return new Promise((resolve, reject)=> {
            axios.post(getApiEndpoint() + 'user/logon', user)
                .then(({ data }) => {
                    this.authenticate(data.value, data.expiresIn);
                    
                    resolve("Logged in successfuly!");
                })
                .catch(({ response }) => {
                    reject(response.data)
                });
        });
    },

    authenticate(tokenStr, expDate) {
        let token = { value: tokenStr, expirationDate: Date.now() + expDate * 60 * 1000 }
        localStorage.setItem('token', JSON.stringify(token));
    },

    isAuthenticated() {
        let tokenJson = localStorage.getItem('token');
        if (!tokenJson) {
            return false;
        }
        let token = JSON.parse(tokenJson);
        if(Date.now() > token.expirationDate) {
            localStorage.removeItem('token');
            return false;
        }
        return true;
    },

    register(user) {
        let data = {    //doing this cause asp.net complains about sending id in database
            username: user.username,
            password: user.password
        }
        return new Promise((resolve, reject) => {
            axios.post(getApiEndpoint() + 'user', data).then(
                (success) => {
                    resolve('You registered successfuly.');
                }, 
                ({ response }) => {
                    reject(response.data);
                })
        });
    },

    logout() {
        localStorage.removeItem('token');
        router.push('/');
        window.location.reload();
    }

}
