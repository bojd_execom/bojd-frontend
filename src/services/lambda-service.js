import { getApiEndpoint } from "../constants";
import axios from 'axios';
import { Lambda } from '../models/lambda.js';

export default {

    create(lambdaInfo, file, triggerType, trigger) {
        let headers = { 
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value 
        };
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append('file', file);
            formData.append('name', lambdaInfo.lambdaName);
            formData.append('language', lambdaInfo.technology);
            formData.append('type', triggerType);
            formData.append('trigger', trigger);
            axios.post(getApiEndpoint() + 'lambda', formData, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    edit(lambdaInfo, file) {
        let headers = { 
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value 
        };
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            formData.append('file', file);
            axios.put(getApiEndpoint() + 'lambda/' + lambdaInfo.id, formData, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    getAll() {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value };
        return new Promise((resolve, reject) => {
            axios.get(getApiEndpoint() + 'lambda', { headers: headers })
                .then(({ data }) => {
                    let lambdas = []
                    for(let lambdaJson of data) {
                        lambdas.push(new Lambda(lambdaJson));
                    }
                    resolve(lambdas);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },

    get(id) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise(resolve => {
            axios.get(getApiEndpoint() + 'lambda/' + id, { headers: headers })
                .then(({ data }) => {
                    resolve(new Lambda(data));
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        })
    },

    delete(id) {
        let headers = { 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')).value }
        return new Promise((resolve, reject) => {
            axios.delete(getApiEndpoint() + 'lambda/' + id, { headers: headers })
                .then(response => {
                    resolve(response);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    },
}
