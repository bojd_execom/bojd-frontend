import { Bucket } from '../models/bucket.js';

const buckets = [
    {
        id: 1,
        name: 'First bucket',
        size: 1,
        spaceLeft: 0.7
    },
    {
        id: 2,
        name: 'Second bucket',
        size: 1,
        spaceLeft: 0.46
    },
    {
        id: 3,
        name: 'Third bucket',
        size: 5,
        spaceLeft: 0.25
    }
]

export default {
    getAllBuckets() {
        return new Promise(resolve => {
            resolve(buckets)
        });
    },

    get(id) {
        return new Promise(resolve => {
            resolve(buckets.filter(bucket => bucket.id == id)[0]);
        })
    }

}