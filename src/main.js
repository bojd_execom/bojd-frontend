import Vue from 'vue';
import App from './App.vue';
import 'bulma/css/bulma.css';
import Toaster from 'v-toaster';
import 'v-toaster/dist/v-toaster.css';

new Vue({
  el: '#app',
  render: h => h(App)
})

Vue.use(Toaster, {timeout: 5000})
